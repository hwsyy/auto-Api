import configparser

cf = configparser.ConfigParser()
cf.read('proxy.conf', encoding='utf-8')
LISTEN_PORT = cf.getint('server', 'LISTEN_PORT')
GOAL_PORT = cf.getint('server', 'GOAL_PORT')

OUTER_URL = cf.get('server', 'OUTER_URL')[1:-1]
MONGODB_DB = cf.get('mongodb', 'DB')[1:-1]  # 去除引号
MONGODB_HOST = cf.get('mongodb', 'HOST')[1:-1]
GOAL_URL = cf.get('server', 'GOAL_URL')[1:-1]

OAUTH2_key = cf.get('secret', 'OAUTH2_key')[1:-1]
OAUTH2_secret = cf.get('secret', 'OAUTH2_secret')[1:-1]
OAUTH2_URL = cf.get('secret', 'OAUTH2_URL')[1:-1]
OAUTH2_OPEN = cf.get('secret', 'OAUTH2_OPEN')[1:-1] == 'True'
SECRET_KEY = cf.get('secret', 'SECRET_KEY')[1:-1]
WEB_CONSOLE_ADDRESS = cf.get('web_console', 'ADDRESS')[1:-1]
WEB_CONSOLE_PORT = cf.getint('web_console', 'PORT')

MODEL_PATH = '/www/coding-220/ginger'
IMPORT_CODE = 'from ginger import app'  # 导入 flask app 的代码 ，比如 from . import app
