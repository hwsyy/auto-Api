import functools

from flask import url_for, session, redirect
from .config import OAUTH2_OPEN


def need_login(f):
    @functools.wraps(f)
    def decorate(*p1, **p2):
        if 'gitlab_token' in session or not OAUTH2_OPEN:
            return f(*p1, **p2)
        else:
            return redirect(url_for('login'))

    return decorate
