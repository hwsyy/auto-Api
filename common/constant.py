class QuestionType:
    IS_LOGIN = 'IS_LOGIN'
    HAS_GROUP = 'HAS_GROUP'
    PARAM_IS_NECESSARY = 'PARAM_IS_NECESSARY'
    PARAM_HAS_GROUP = 'PARAM_HAS_GROUP'
    PARAM_RANGE = 'PARAM_RANGE'
    ABOUT_SQL = 'ABOUT_SQL'
    API_APPLICATION = 'API_APPLICATION'  # 注释
    PARAM_TYPE = 'PARAM_TYPE'
    API_TYPE = 'API_TYPE'
    PARAM_RANGE_TYPE = 'PARAM_RANGE_TYPE'


class ApiType:
    SELECT = 'SELECT'
    UPDATE = 'UPDATE'
    INSERT = 'INSERT'


class RangeType:
    SP_LEN = 'sp_len'
    VAR_LEN = 'var_len'
    CONFLICT = 'conflict'
    ENUM = 'enum'
    FLOW = 'flow'
    UNBOUND = 'unbound'
    RANGE = 'range'
    HISTORY = 'history'


class GroupType:
    SELECT_ONE = 'select_one'  # n 选一
    BINDING = 'binding'  # 必须同时出现


class IntCheck:
    @classmethod
    def check(cls, value):
        try:
            int(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'int'


class FloatCheck:
    @classmethod
    def check(cls, value):
        try:
            float(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'float'


class BoolCheck:
    @classmethod
    def check(cls, value):
        return value in ['True', 'False']

    def __str__(self):
        return 'bool'


class StrCheck:
    @classmethod
    def check(cls, value):
        try:
            str(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'str'


BaseTypeList = [IntCheck, FloatCheck, BoolCheck, StrCheck]

QuestionComment = {
    QuestionType.IS_LOGIN: "是否需要登陆",
    QuestionType.PARAM_HAS_GROUP: "参数是否有分组，分组情况是怎么样的",
    QuestionType.PARAM_IS_NECESSARY: "确认参数的必要性",
    QuestionType.ABOUT_SQL: "API影响到的数据库",
    QuestionType.API_APPLICATION: "API的注释是什么",
    QuestionType.HAS_GROUP: "访问权限组",
    QuestionType.PARAM_RANGE: "各个参数的取值范围",
    QuestionType.PARAM_RANGE_TYPE: '参数取值范围的类型',
    QuestionType.PARAM_TYPE: "各个参数的类型",
    QuestionType.API_TYPE: "API的类型（查询，修改，新增）",
}
