import sqlparse
from sqlparse.sql import IdentifierList, Identifier
from sqlparse.tokens import Keyword, DML

from common.documents import APIModel
from itertools import chain
import itertools


def parse(content):
    """
    解析sql内感兴趣的内容
    :param content:
    :return: [表->语句类型]
    """
    parsed = sqlparse.parse(content)
    return parsed[0].get_type(), extract_tables(content)


def get_data_by_route(data, route):
    td = data
    for i, r in enumerate(route):
        td = td[r]
    return td


def get_data_route(data):
    """获取参数的路由"""
    if isinstance(data, dict):
        for key in data:
            t = [key]
            t.extend(get_data_route(data[key]))
            yield t
    if isinstance(data, list):
        for i in range(len(data)):
            t = [i]
            t.extend(get_data_route(data[i]))
            yield t
    return []


def get_data_by_routes(data, routes, all_have=True):
    """

    :param data:
    :param routes:
    :param all_have: 所有的路由都要获取到
    :return:
    """
    res = type(data)()
    for route in routes:
        try:

            tr = res
            td = data
            for i, r in enumerate(route):
                if i < len(route) - 1:
                    tr[r] = type(td[r])()
                    tr = tr[r]
                    td = td[r]
                else:
                    tr[r] = td[r]
        except:
            if all_have:
                return None
    return res


def get_all_combinations(items, s_to_m=True):
    if s_to_m:
        ite = range(1, len(items))
    else:
        ite = range(len(items) - 1, 0, -1)
    for i in ite:
        yield from itertools.combinations(items, i)


def get_model_struct():
    # 获取模型的url结构
    res = {}
    for item in APIModel.objects():
        tr = res
        url = item.get_relate_url().split('/')
        if len(url) < 3:
            url += [''] * (3 - len(url))
        lv = 0
        for i in chain(url[:2], ['/'.join(url)]):
            lv += 1
            if lv == 3:
                tr[i] = item
            else:
                if i not in tr:
                    tr[i] = {}
            tr = tr[i]
    # 最高3级
    return res


def is_subselect(parsed):
    if not parsed.is_group:
        return False
    for item in parsed.tokens:
        if item.ttype is DML and item.value.upper() == 'SELECT':
            return True
    return False


def extract_from_part(parsed):
    from_seen = False
    for item in parsed.tokens:
        if from_seen:
            if is_subselect(item):
                for x in extract_from_part(item):
                    yield x
            elif item.ttype is Keyword:
                raise StopIteration
            else:
                yield item
        elif item.ttype is Keyword and item.value.upper() == 'FROM':
            from_seen = True


def get_outer_identifiers_name(token_stream):
    yield from [item.get_name() for item in token_stream if isinstance(item, Identifier)]


def extract_table_identifiers(token_stream):
    for item in token_stream:
        if isinstance(item, IdentifierList):
            for identifier in item.get_identifiers():
                yield identifier.get_name()
        elif isinstance(item, Identifier):
            yield item.get_name()
        # It's a bug to check for Keyword here, but in the example
        # above some tables names are identified as keywords...
        elif item.ttype is Keyword:
            yield item.value


def extract_tables(sql):
    stream = extract_from_part(sqlparse.parse(sql)[0])
    return list(extract_table_identifiers(stream))


def extract_definitions(token_list):
    # assumes that token_list is a parenthesis
    definitions = []
    tmp = []
    # grab the first token, ignoring whitespace. idx=1 to skip open (
    tidx, tkl = token_list.token_next(1)
    while tkl and not tkl.match(sqlparse.tokens.Punctuation, ')'):
        if isinstance(tkl, IdentifierList):
            tl = list(tkl)
        else:
            tl = [tkl]
        tidx, tkl = token_list.token_next(tidx, skip_ws=False)
        for token in tl:
            if token and token.match(sqlparse.tokens.Punctuation, ','):
                definitions.append(tmp)
                tmp = []
                continue
            if token.value.strip():
                tmp.append(token)
            # grab the next token, this times including whitespace
            # split on ",", except when on end of statement

    if tmp and isinstance(tmp[0], sqlparse.sql.Identifier):
        definitions.append(tmp)
    return definitions





if __name__ == '__main__':
    get_model_struct()
