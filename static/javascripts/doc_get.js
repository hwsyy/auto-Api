function auto_solved_one(url) {
    $.ajax({
        type: 'POST',
        url: "/auto_solved_one",
        data: {model_url: url},
        success: function (resp) {
            get_doc(url);
        }
    });
    alert("开始自动解答")
}

function get_doc(url) {
    $.ajax({
        type: 'POST',
        url: "/doc",
        data: {model_url: url},
        success: function (resp) {
            $("#content").html(resp);
            $('.tree').treegrid();
        }
    });
}

function build_model() {
    $.ajax({
        type: 'GET',
        url: "/build_model"
    });
    alert("正在构建模型，所花费的时间根据数据量变化，请耐心等待")
}
function solve_one(key){
    $.ajax({
        type: 'POST',
        url: "/solve_one",
        data: {que_key: key},
        success: function (resp) {
            get_doc(resp)
        }
    });
    alert("正在自动解决中，请稍等")
}

function remove_ans(ans_key) {
    $.ajax({
        type: 'POST',
        url: "/remove_ans",
        data: {que_key: ans_key},
        success: function (resp) {
            get_doc(resp)
        }
    });
}
function search_result(){
    var text = $("#q_header").val();
    $.ajax({
        type: 'POST',
        url: "/search_results",
        data: {q:text},
        success: function (resp) {
            $("#content").html(resp);
        }
    });
    return false
}
function save_ans(index) {
    var data = $("#form" + index).serialize();
    $.post("save_answer", data ,function (resp) {
        get_doc(resp);
    });
}

function auto_solve() {
    $.ajax({
        type: 'GET',
        url: "/auto_solve"
    });
    alert("正在自动解决问题当中...")
}

function update_by_answers() {
    $.ajax({
        type: 'GET',
        url: "/update_by_answers",
        success: function (resp) {
            alert("更新完成")
        }
    });
}

function set_ignore(url) {
    $.ajax({
        type: 'POST',
        url: "/set_ignore",
        data: {model_url: url},
        success: function (resp) {
            var but = $("#set_ignore");
            if (but.text().includes("取消")) {
                but.text("忽略");
                but.addClass("btn-success");
            } else {
                but.text("取消忽略");
                but.removeClass("btn-success");
            }
        }
    });
}