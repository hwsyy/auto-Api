import hashlib
import re
from contextlib import closing

import requests
from flask import Flask, request, Response

from common.config import LISTEN_PORT, GOAL_PORT, GOAL_URL
from common.documents import Ask

app = Flask(__name__)


@app.before_request
def before_request():
    from common.documents import AskProxySetting
    md = hashlib.md5()
    url = request.url
    method = request.method
    data = request.form or request.data or None
    headers = dict()
    goalurl = re.sub("://(.*?)/", "://{}:{}/".format(GOAL_URL, GOAL_PORT), url)
    for name, value in request.headers:
        if name == 'Content-Type':
            if 'form-data' in value:
                continue
        headers[name] = value
    ask = Ask(url=goalurl, method=method, data=data, headers=headers, source_type='原始请求')
    files = {}

    for k in request.files:
        files[k] = (request.files[k].filename, request.files[k], request.files[k].content_type)
        request.files[k].seek(0)

    md.update(goalurl.encode())
    md.update(method.encode())
    if AskProxySetting.md_use_data():
        md.update(str(data).encode())
    if AskProxySetting.md_use_header():
        md.update(str(headers).encode())
    with closing(
            requests.request(method, goalurl, headers=headers, data=data,
                             files=files,
                             allow_redirects=False)
    ) as r:
        resp_headers = []
        for name, value in r.raw.headers.items():
            resp_headers.append((name, value))
        if AskProxySetting.is_on_listen():

            ask.resp_content = r.content
            ask.resp_header = resp_headers
            ask.resp_stat = r.status_code
            if AskProxySetting.md_use_result():
                md.update(ask.resp_content)
            ask.hash_code = md.hexdigest()
            ask.has_file = len(request.files) > 0
            try:
                import json
                json.loads(r.content.decode())
                ask.save()
            except Exception:
                pass
        return Response(r, status=r.status_code, headers=resp_headers)


app.run(host='0.0.0.0', port=LISTEN_PORT, debug=True)
